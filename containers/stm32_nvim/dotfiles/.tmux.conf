# Hierarchy:
#  Server
#  ㄴSession
#    ㄴWindow
#      ㄴPane

# Options:
# - Session options (set-option [-g])
# - Window options (set-window-option [-g])


# -------------------------------------------------------------------
# Session options
# -------------------------------------------------------------------
# Change bind key to ctrl-a
unbind-key c-b
set-option -g prefix c-a

# Index starts from 1
set-option -g base-index 1

# Renumber windows when a window is closed
set-option -g renumber-windows on

# History
set-option -g history-limit 102400

# Repeat time limit (ms)
set-option -g repeat-time 0

# Time before passing escape to app
set -sg escape-time 1

# 256-color terminal
set-option -g default-terminal "xterm-256color"

# Add truecolor support (tmux info | grep Tc)
set-option -g terminal-overrides "xterm-256color:Tc"

# Key binding in the status line (bind-key :)
set-option -g status-keys vi

# Mouse
set-option -g mouse on

# Window title
set -g set-titles on
set -g set-titles-string "#H > #S > #W"

# -------------------------------------------------------------------
# Window options
# -------------------------------------------------------------------
# Copy-mode
set-window-option -g mode-keys vi

# -------------------------------------------------------------------
# Key bindings
# -------------------------------------------------------------------
# prefix c
bind-key c new-window -c "#{pane_current_path}"

# prefix ctrl-a
bind-key c-a last-window

# prefix a
bind-key a send-prefix

# prefix |
bind-key | split-window -h -c "#{pane_current_path}"

# prefix -
bind-key - split-window -c "#{pane_current_path}"

# Moving windows
bind-key -r > swap-window -t :+
bind-key -r < swap-window -t :-

# Back and forth
bind-key bspace previous-window
bind-key space next-window
bind-key / next-layout # Overridden

# Pane-movement
bind-key h select-pane -L
bind-key l select-pane -R
bind-key j select-pane -D
bind-key k select-pane -U
bind-key tab select-pane -t :.+
bind-key btab select-pane -t :.-

# Movin panes
unbind-key m
bind-key m command-prompt -p "send pane to:"  "join-pane -t '%%'"
bind-key M choose-tree -Zw "join-pane -t '%%'"
bind-key C-m choose-tree -Zs "join-pane -t '%%'"

# Fast pane movement
bind-key -n M-k resize-pane -U 2
bind-key -n M-j resize-pane -D 2
bind-key -n M-h resize-pane -L 2
bind-key -n M-l resize-pane -R 2

# Synchronize panes
bind-key * set-window-option synchronize-pane

# Reload ~/.tmux.conf
bind-key r source-file ~/.tmux.conf \; display-message "Reloaded!"

# copy-mode
bind-key -r Escape copy-mode
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi C-v send-keys -X rectangle-toggle \; send -X begin-selection
bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "xclip -in -selection clipboard"
bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "xclip -in -selection clipboard"

# Capture pane and open in Vim
bind-key C-c run 'tmux capture-pane -S -102400 -p > /tmp/tmux-capture.txt'\;\
             new-window "nvim -R /tmp/tmux-capture.txt"

# Clear scrollback buffer
bind-key C-l send-keys C-l \; clear-history

# Hide or show statusbar
bind-key G set status

# -------------------------------------------------------------------
# Decoration (256-color)
# -------------------------------------------------------------------
# Status bar {
    set-option -g status-justify left
    set-option -g status-left '#[bg=colour72] #S #[bg=colour236] #[fg=colour143]#h #[bg=colour237] '
    set-option -g status-left-length 99
    set-option -g status-style 'bg=colour237 fg=colour236'
    set-option -g status-right '#[bg=colour236]#[fg=colour143] #(date "+%a %b %d %H:%M") #[bg=colour72] '
    set-option -g status-interval 60

    set-window-option -g window-status-format '#[bg=colour240]#[fg=colour107] #I #[fg=colour110]#[bg=colour240]#W#[bg=colour240]#[fg=colour195]#F#[bg=colour240] '
    set-window-option -g window-status-current-format '#[bg=colour235]#[fg=colour215] #I#[bg=colour235]#[fg=colour167] #[bg=colour235]#W#[bg=colour235]#[fg=colour195]#F '
# }

# The panes {
    set -g window-style bg=colour238
    set -g window-active-style bg=colour239

    set -g pane-border-style 'bg=colour238 fg=colour95'
    set -g pane-active-border-style 'bg=colour238 fg=colour109'
# }

# Messages {
    set -g message-style 'fg=colour252 bg=colour23'
    set -g message-command-style 'fg=colour252 bg=colour52'
#}

# loud or quiet?
set-option -g visual-activity off
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none

# -------------------------------------------------------------------
# fzf integration
# -------------------------------------------------------------------
# Tmux completion
# bind-key -n 'M-t' run "tmux split-window -p 40 'tmux send-keys -t #{pane_id} \"$(tmuxwords.rb --all --scroll 1000 --min 5 | fzf --multi | paste -sd\\  -)\"'"

# fzf-locate (all)
# bind-key -n 'M-`' run "tmux split-window -p 40 'tmux send-keys -t #{pane_id} \"$(locate / | fzf -m | paste -sd\\  -)\"'"

# select-pane (@george-b)
# bind-key 0 run "tmux split-window -l 12 'zsh -ci ftpane'"
