# Load Prezto
source "${ZSHUTILDIR}/.zprezto/init.zsh"

###############################################################################
#                             Completion options                              #
###############################################################################
_comp_options+=(globdots)
fpath=(~/.config/zsh/completion $fpath)

###############################################################################
#                                    Alias                                    #
###############################################################################
alias ls="ls -A --color"
alias vim="nvim"

##########
#  tmux  #
##########
alias t='tmux'
alias tp='tmuxp load'
alias ta='tmux attach -t'
alias tad='tmux attach -d -t'
alias ts='tmux new-session -s'
alias tl='tmux list-sessions'
alias tksv='tmux kill-server'
alias tkss='tmux kill-session -t'

###############################################################################
#                                    Bind                                     #
###############################################################################
bindkey -v
typeset -g -A key

# speed up mode switching for vi-mode
KEYTIMEOUT=20

# allow ctrl-p, ctrl-n for navigate history (standard behaviour)
bindkey -M viins '^p' history-substring-search-up
bindkey -M viins '^n' history-substring-search-down

# some emacs bindings for the insert mode
bindkey -M viins "$key_info[Control]E" vi-end-of-line
bindkey -M viins "$key_info[Control]A" vi-beginning-of-line

# some normal mode commands
bindkey -M vicmd 'H' run-help


# don't allow esc to hang
bindkey -M vicmd '^[' undefined-key
bindkey -rM viins '^X'
bindkey -M viins '^X,' _history-complete-newer \
                 '^X/' _history-complete-older \
                 '^X`' _bash_complete-word
# edit in editor
bindkey -M vicmd '^E' edit-command-line
bindkey -M viins '^X^E' edit-command-line

##############
#  surround  #
##############

# add quote and bracket text-objects

autoload -U select-quoted; zle -N select-quoted
for m in visual viopp; do
    for c in {a,i}{\',\",\`}; do
        bindkey -M $m $c select-quoted
    done
done

autoload -U select-bracketed; zle -N select-bracketed
for m in visual viopp; do
    for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
        bindkey -M $m $c select-bracketed
    done
done

autoload -Uz surround
zle -N delete-surround surround
zle -N add-surround surround
zle -N change-surround surround
bindkey -a cs change-surround
bindkey -a ds delete-surround
bindkey -a ys add-surround
bindkey -M visual S add-surround

#########################
#  menuselect bindings  #
#########################

zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect ' ' vi-cmd-mode


###############################################################################
#                                   History                                   #
###############################################################################
HISTFILE=~/.hist/zshhistfile
HISTSIZE=10000
SAVEHIST=10000

# immediately save history file
setopt inc_append_history
# readback history
# setopt share_history

###############################################################################
#                                   Plugins                                   #
###############################################################################

#########
#  fzf  #
#########

export FZF_TMUX=1

FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden '
FZF_DEFAULT_COMMAND+='--follow -g "!{.git,node_modules}/*" 2>/dev/null'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

FZF_PREVIEW_CMD='(highlight -O ansi -l {} || cat {} || tree -C {}) '
FZF_PREVIEW_CMD+='2> /dev/null | head -n 200'
export FZF_CTRL_T_OPTS="--preview '${FZF_PREVIEW_CMD}'"

export FZF_ALT_C_COMMAND="bfs -type d 2>/dev/null"
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -n 200'"

source /usr/share/doc/fzf/examples/key-bindings.zsh

##############################
#  fast-syntax-highlighting  #
##############################
source "${ZSHUTILDIR}/.fsh/fast-syntax-highlighting.plugin.zsh"

###############################################################################
#                                  Functions                                  #
###############################################################################

##########################################
#  double esc to toggle sudo / sudoedit  #
##########################################
sudo-command-line() {
	[[ -z $BUFFER ]] && zle up-history
	if [[ $BUFFER == sudo\ * ]]; then
		LBUFFER="${LBUFFER#sudo }"
	elif [[ $BUFFER == $EDITOR\ * ]]; then
		LBUFFER="${LBUFFER#$EDITOR }"
		LBUFFER="sudoedit $LBUFFER"
	elif [[ $BUFFER == sudoedit\ * ]]; then
		LBUFFER="${LBUFFER#sudoedit }"
		LBUFFER="$EDITOR $LBUFFER"
	else
		LBUFFER="sudo $LBUFFER"
	fi
}
zle -N sudo-command-line
# Defined shortcut keys: [Esc] [Esc]
bindkey "\e\e" sudo-command-line

###############################################################################
#                                   rehash                                    #
###############################################################################

# rehash commands
alias zrh="rehash"
alias rehash-all="pkill zsh --signal=USR1"
alias zrha="rehash-all"

# trap rehash signal sent by a pacman hook (or issued manually)
catch_signal_usr1() {
	trap catch_signal_usr1 USR1
	if [[ -o INTERACTIVE ]]; then
		rehash
	fi
}
trap catch_signal_usr1 USR1

###############################################################################
#                       reload configs and completions                        #
###############################################################################

alias zcr="rm -f ~/.zcompdump; compinit"

zsh_reload_config() {
	printf '\nExecuting a new shell instance.\n' 1>&2
	exec "${SHELL}"
}

alias zrc='zsh_reload_config'

# trap config reload signal
catch_signal_usr2() {
	trap catch_signal_usr2 USR2
	if [[ -o INTERACTIVE ]]; then
		zsh_reload_config
		rm -f ~/.zcompdump
		compinit
	fi
}
trap catch_signal_usr2 USR2

alias zrca="pkill zsh --signal=USR2"
